import request from '@/utils/request'

/**
* 生成配置分页查询
* @param {查询条件} data
*/
export function listGenConfig(query) {
  return request({
    url: 'system/GenConfig/list',
    method: 'get',
    params: query,
  })
}

/**
* 新增生成配置
* @param data
*/
export function addGenConfig(data) {
  return request({
    url: 'system/GenConfig',
    method: 'post',
    data: data,
  })
}
/**
* 修改生成配置
* @param data
*/
export function updateGenConfig(data) {
  return request({
    url: 'system/GenConfig',
    method: 'PUT',
    data: data,
  })
}
/**
* 获取生成配置详情
* @param {Id}
*/
export function getGenConfig(id) {
  return request({
    url: 'system/GenConfig/' + id,
    method: 'get'
  })
}

/**
* 删除生成配置
* @param {主键} pid
*/
export function delGenConfig(pid) {
  return request({
    url: 'system/GenConfig/' + pid,
    method: 'delete'
  })
}
